import wx
import os
import pandas as pd
import os
from Finance import empresa
from datetime import datetime


class MyApp(wx.App):
    def __init__(self):
        super().__init__(clearSigInt=True)

        # marco de inicio
        self.InitFrame()

    def InitFrame(self):
        frame = MyFrame()
        frame.Show()


class MyFrame(wx.Frame):
    def __init__(self, title="Finance App", pos=(100, 100)):
        super().__init__(None, title=title, pos=pos)
        # initialize the frame's contents
        self.OnInit()

    def OnInit(self):
        self.panel = MyForm(self)
        self.Fit()



class MyForm(wx.Panel):

    def __init__(self, parent):
        self.info = []
        super().__init__(parent=parent)

        # Agregue un panel para que se vea correcto en todas las plataformas


        bmp = wx.ArtProvider.GetBitmap(id=wx.ART_INFORMATION,
                                       client=wx.ART_OTHER, size=(16, 16))


        titleIco = wx.StaticBitmap(self, wx.ID_ANY, bmp)
        title = wx.StaticText(self, wx.ID_ANY, 'Elija una Empresa')
        inputOneIco = wx.StaticBitmap(self, wx.ID_ANY, bmp)


        labelOne = wx.StaticText(self, wx.ID_ANY, 'Opciones')
        self.inputOne = wx.Choice(self, choices=['APPLE', 'DISNEY', 'GMC', 'NETFLIX','SHELL'])


        labelTwo = wx.StaticText(self, wx.ID_ANY, 'Oprima un Boton')

        promDiarioBtn = wx.Button(self, wx.ID_ANY, 'Promedio Diario')
        promAnualBtn = wx.Button(self, wx.ID_ANY, 'Promedio Anual')
        valHistoricosBtn = wx.Button(self, wx.ID_ANY, 'Valores Historicos')
        imprimirBtn = wx.Button(self, wx.ID_ANY, 'Imprimir')
        salirBtn = wx.Button(self, wx.ID_ANY, 'Salir')
        f = open("Resultados.txt", "a")
        f.write('Consultas del dia: '+ str( datetime.today().strftime('%Y-%m-%d %H:%M'))+'\n')
        f.close()

        self.Bind(wx.EVT_BUTTON, self.promDiario, promDiarioBtn)
        self.Bind(wx.EVT_BUTTON, self.promAnual, promAnualBtn)
        self.Bind(wx.EVT_BUTTON, self.valHistoricos, valHistoricosBtn)

        self.Bind(wx.EVT_BUTTON, self.imprimir, imprimirBtn)
        self.Bind(wx.EVT_BUTTON, self.salir, salirBtn)

        #aca creo el marco principal y secundario
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        titleSizer = wx.BoxSizer(wx.HORIZONTAL)

        inputOneSizer = wx.BoxSizer(wx.HORIZONTAL)
        inputTwoSizer = wx.BoxSizer(wx.HORIZONTAL)
        inputThreeSizer = wx.BoxSizer(wx.HORIZONTAL)
        inputFourSizer = wx.BoxSizer(wx.HORIZONTAL)
        inputFiveSizer = wx.BoxSizer(wx.HORIZONTAL)


        submitBtnSizer = wx.BoxSizer(wx.HORIZONTAL)
        titleSizer.Add(titleIco, 0, wx.ALL, 5)
        titleSizer.Add(title, 0, wx.ALL, 5)

        inputOneSizer.Add(inputOneIco, 0, wx.ALL, 5)
        inputOneSizer.Add(self.inputOne, 1, wx.ALL | wx.EXPAND, 5)
        inputOneSizer.Add(labelOne, 0, wx.ALL, 5)
        inputTwoSizer.Add(labelTwo, 0, wx.ALL, 5)
        inputThreeSizer.Add(promDiarioBtn, 0, wx.ALL, 5)
        inputFourSizer.Add(promAnualBtn, 0, wx.ALL, 5)
        inputFiveSizer.Add(valHistoricosBtn, 0, wx.ALL, 5)

        #Marco Secundario
        submitBtnSizer.Add(imprimirBtn, 0, wx.ALL, 5)
        submitBtnSizer.Add(salirBtn, 0, wx.ALL, 5)

        mainSizer.Add(titleSizer, 0, wx.CENTER)
        mainSizer.Add(inputOneSizer, 0, wx.ALL | wx.EXPAND, 5)
        mainSizer.Add(inputTwoSizer, 0, wx.ALL | wx.EXPAND, 5)
        mainSizer.Add(inputThreeSizer, 0, wx.ALL | wx.EXPAND, 5)
        mainSizer.Add(inputFourSizer, 0, wx.ALL | wx.EXPAND, 5)
        mainSizer.Add(inputFiveSizer, 0, wx.ALL | wx.EXPAND, 5)
        mainSizer.Add(wx.StaticLine(self), 0, wx.ALL | wx.EXPAND, 5)
        mainSizer.Add(submitBtnSizer, 0, wx.ALL | wx.CENTER, 5)

        self.SetSizer(mainSizer)
        mainSizer.Fit(self)
        self.Layout()

    def promDiario(self, event):
        #me permite elegir el dia y calcula el promedio entre el Open y Close
        empresa_df = empresa(self, event)
        lista = self.getData()
        nombre = lista[0]
        f = open("Resultados.txt", "a")
        f.write("Calculamos el Promedio diario de la empresa "+ str(nombre) +' que es: U$')
        f.close()

        class OtroFrame2(wx.Frame):
            def __init__(self, empresa_df):
                self.empresa_df = empresa_df
                super().__init__(parent=None, title='Respuesta')

                panel = wx.Panel(self)
                my_sizer = wx.GridBagSizer(5, 7)

                mensaje = wx.StaticText(panel, wx.ID_ANY, 'Ingrese la fecha que desee consultar')
                my_sizer.Add(mensaje, pos=(2, 0))



                dia = wx.StaticText(panel, wx.ID_ANY, 'Dia')
                my_sizer.Add(dia, pos=(3, 1))
                self.seleccionDia = wx.Choice(panel,
                                              choices=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
                                                       '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23',
                                                       '24', '25', '26', '27', '28', '29', '30', '31'])
                my_sizer.Add(self.seleccionDia, pos=(3, 2))

                mes = wx.StaticText(panel, wx.ID_ANY, 'Mes')
                my_sizer.Add(mes, pos=(3, 3))
                self.seleccionMes = wx.Choice(panel,
                                              choices=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'])
                my_sizer.Add(self.seleccionMes, pos=(3, 4))

                año = wx.StaticText(panel, wx.ID_ANY, 'Año')
                my_sizer.Add(año, pos=(3, 5))
                self.seleccionAño = wx.Choice(panel, choices=['2020', '2021'])
                my_sizer.Add(self.seleccionAño, pos=(3, 6))

                calcularBtn = wx.Button(panel, wx.ID_ANY, 'Calcular')
                my_sizer.Add(calcularBtn, pos=(4, 0))

                self.Bind(wx.EVT_BUTTON, self.promedioDia, calcularBtn)


                panel.SetSizer(my_sizer)
                my_sizer.Fit(self)
                self.Show()

            def getData(self):
                data = []

                selectionD = self.seleccionDia.GetSelection()
                selectionM = self.seleccionMes.GetSelection()
                selectionA = self.seleccionAño.GetSelection()
                data.append(
                    self.seleccionDia.GetString(selectionD)
                )
                data.append(
                    self.seleccionMes.GetString(selectionM)
                )
                data.append(
                    self.seleccionAño.GetString(selectionA)
                )

                return data

            def promedioDia(self, event):
                lista = self.getData()
                dia = str(lista[0])
                mes= str(lista[1])
                año = str(lista [2])
                fecha = año +'/'+ mes +'/'+ dia
                fecha_dt = datetime.strptime(str(fecha), '%Y/%m/%d')
                mask21 = self.empresa_df['Date'] == fecha_dt
                filtered_df21 = empresa_df.loc[mask21].head(3)

                try:
                    promedio = float(filtered_df21['Open']+filtered_df21['Close'])/2
                    print(filtered_df21)
                    print(promedio)
                except (UnboundLocalError, TypeError):
                    respuesta2 = wx.StaticText(self, wx.ID_ANY, 'No existen datos ese dia!, elija otro')

                respuesta1 = wx.StaticText(self, wx.ID_ANY, 'El promedio del dia es : U$' + str(round(promedio,2)))

                f = open("Resultados.txt", "a")
                f.write(str(round(promedio,2))+ '\n')
                f.close()

        if __name__ == '__main__':
            app = wx.App()
            frame = OtroFrame2(empresa_df)
            app.MainLoop()

    def promAnual(self,event):
        #selecciono el año y calculo el promedio anual empleando la columna Close
        empresa_df = empresa(self, event)
        lista = self.getData()
        nombre = lista[0]

        f = open("Resultados.txt", "a")
        f.write("Calculamos el Promedio anual de la empresa " + str(nombre) + ' que es: U$')
        f.close()

        mask20 = (empresa_df['Date'] > '2020-01-01') & (empresa_df['Date'] <= '2020-12-31')
        filtered_df20 = empresa_df.loc[mask20]
        promedio20 = filtered_df20['Close'].mean()

        mask21 = (empresa_df['Date'] > '2021-01-01') & (empresa_df['Date'] <= '2021-12-31')
        filtered_df21 = empresa_df.loc[mask21]
        promedio21 = filtered_df21['Close'].mean()

        listaDePromedios= []
        listaDePromedios.append(round(promedio20))
        listaDePromedios.append(round(promedio21))

        class OtroFrame2(wx.Frame):
            def __init__(self, listaDePromedios):
                super().__init__(parent=None, title='Respuesta')

                panel = wx.Panel(self)
                my_sizer = wx.GridBagSizer(5, 5)

                respuesta = wx.StaticText(panel, wx.ID_ANY, 'Ingrese el Año que desee consultar')
                my_sizer.Add(respuesta, pos=(2, 0))

                self.seleccion = wx.Choice(panel, choices=['2020','2021'])
                my_sizer.Add(self.seleccion, pos=(2, 1))

                calcularBtn = wx.Button(panel, wx.ID_ANY, 'Calcular')
                my_sizer.Add(calcularBtn, pos=(4, 0))

                self.Bind(wx.EVT_BUTTON, self.promedioAnual, calcularBtn)

                panel.SetSizer(my_sizer)
                #my_sizer.Fit(self)
                self.Show()
            def getData(self):
                data = []

                selection = self.seleccion.GetSelection()
                data.append(
                    self.seleccion.GetString(selection)
                )

                return data
            def promedioAnual(self,event):
                lista = self.getData()
                Año = lista[0]
                print(Año)
                if Año == '2020':
                    respuesta2 = wx.StaticText(self, wx.ID_ANY, 'El promedio Anual es : U$' + str(listaDePromedios[0]))
                    f = open("Resultados.txt", "a")
                    f.write(str(listaDePromedios[0]) + ' Correspondiente al Anio: '+str(Año) +'\n')

                    f.close()

                else:
                    respuesta2 = wx.StaticText(self, wx.ID_ANY, 'El promedio Anual es : U$' + str(listaDePromedios[1]))
                    f = open("Resultados.txt", "a")
                    f.write(str(listaDePromedios[1]) + ' Correspondiente al Anio: '+str(Año)+'\n')
                    f.close()



        if __name__ == '__main__':
            app = wx.App()
            frame = OtroFrame2(listaDePromedios)
            app.MainLoop()

    def valHistoricos(self,event):
        #me da la opcion de elegir el año y toma los valores mas altos y mas bajos de cierre
        empresa_df = empresa(self, event)
        lista = self.getData()
        nombre = lista[0]

        f = open("Resultados.txt", "a")
        f.write("Calculamos los valores historicos de la empresa " + str(nombre) + ':\n')
        f.close()

        promedio = empresa_df['Adj Close'].mean()
        #busco la fila con el maximo y el minimo
        minimos = empresa_df.loc[empresa_df["Close"].idxmin()]
        maximos = empresa_df.loc[empresa_df["Close"].idxmax()]
        resultados=[]
        resultados.append(minimos['Date'])
        resultados.append(minimos['Close'])
        resultados.append(maximos['Date'])
        resultados.append(maximos['Close'])

        class OtroFrame(wx.Frame):
            def __init__(self, resultados):
                super().__init__(parent=None, title='Respuesta')
                panel = wx.Panel(self)
                my_sizer = wx.GridBagSizer(5, 5)

                respuesta = wx.StaticText(panel, wx.ID_ANY, 'El minimo Anual Historico es del dia: ' + str(
                    resultados[0].strftime('%Y-%m-%d')) + '  y es de: U$' + str(round(resultados[1], 2)))
                my_sizer.Add(respuesta, pos=(0, 0))

                respuesta1 = wx.StaticText(panel, wx.ID_ANY, 'El maximo Anual Historico es del dia: ' + str(
                    resultados[2].strftime('%Y-%m-%d')) + '  y es de: U$' + str(round(resultados[3], 2)))
                my_sizer.Add(respuesta1, pos=(2, 0))

                f = open("Resultados.txt", "a")
                f.write(' El minimo Anual Historico es del dia: ' + str(
                    resultados[0].strftime('%Y-%m-%d')) + '  y es de: U$' + str(round(resultados[1], 2)) + '\n')
                f.write(' El maximo Anual Historico es del dia: ' + str(
                    resultados[2].strftime('%Y-%m-%d')) + '  y es de: U$' + str(round(resultados[3], 2)) + '\n')
                f.close()
                panel.SetSizer(my_sizer)
                my_sizer.Fit(self)
                self.Show()

        if __name__ == '__main__':
            app = wx.App()
            frame = OtroFrame(resultados)
            app.MainLoop()




    def imprimir(self, event,):
        # esta funcion abre el txt donde se guardaron las consultas para imprimir
        os.system('Resultados.txt')


    def salir(self, event):
        #se cierra el programa
        self.closeProgram()



    def closeProgram(self):
        # self.GetParent() will get the frame which
        # has the .Close() method to close the program
        self.GetParent().Close()

    def getData(self):
        '''
        this here will procure data from all buttons
        '''
        data = []

        selection = self.inputOne.GetSelection()
        data.append(
                     self.inputOne.GetString(selection)
                    )

        return data




# Run the program
if __name__ == '__main__':
    app = MyApp()
    app.MainLoop()


