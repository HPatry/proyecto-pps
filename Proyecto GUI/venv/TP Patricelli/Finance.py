import wx
import pandas as pd

def empresa(self ,event):

    lista = self.getData()
    nombre = lista[0]
    if nombre == 'APPLE':
        empresa_df = pd.read_csv('data/APPLE.csv', parse_dates=[0])
    elif nombre == 'DISNEY':
        empresa_df = pd.read_csv('data/DISNEY.csv', parse_dates=[0])
    elif nombre == 'GMC':
        empresa_df = pd.read_csv('data/GMC.csv', parse_dates=[0])
    elif nombre == 'NETFLIX':
        empresa_df = pd.read_csv('data/NETFLIX.csv', parse_dates=[0])
    else:
        empresa_df = pd.read_csv('data/SHELL.csv', parse_dates=[0])


    return empresa_df